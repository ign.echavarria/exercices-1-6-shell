#!/bin/bash
PARAM=$1
[ -f $PARAM ]
case "$?" in
	0)
		echo "$PARAM est un fichier."
		;;
	*)
		[ -d $PARAM ]
		case "$?" in
			0)
				echo "$PARAM est un dossier"
				;;
			*)
				echo "$PARAM est de type inconnu."
				;;
		esac
		;;
esac
