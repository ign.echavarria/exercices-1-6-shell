#!/bin/bash
NOM=$1
[ -f NOM ]
case $? in
	0)
		TAILLE=$(wc -c $NOM 2> /dev/null| cut -d " " -f1)
		echo "$NOM ------- Taille $TAILLE octets"
		;;
	*)
		echo "$NOM n'est pas un fichier"
		;;
esac
