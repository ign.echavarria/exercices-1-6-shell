#!/bin/bash
PARAM_LIST=$@
PARAM_QTY=$#
case "$PARAM_QTY" in
	0)
		echo "Aucun paramètre sur la ligne de commande"
		;;
	*)
		echo "Il y a $PARAM_QTY paramètres sur la ligne de commande"
		echo "$@"
		for PARAM in $PARAM_LIST
		do
			echo "$PARAM"
		done
		;;
esac
