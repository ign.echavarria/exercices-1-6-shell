#§/bin/bash
LISTED=$(ls)
FILE_QTY=0
REP_QTY=0

files_count() {
	for CONTENU in $LISTED
	do
		[ -f $CONTENU ]
		case "$?" in
			0)
				FILE_QTY=$(($FILE_QTY + 1))
				;;
		esac
	done
	echo "Fichiers : $FILE_QTY"
}

rep_count() {
        for CONTENU in $LISTED
        do
                [ -d $CONTENU ]
                case "$?" in
                        0)
                                REP_QTY=$(($REP_QTY + 1))
                                ;;
                esac
        done
        echo "Répertoires: $REP_QTY"
}

files_count
rep_count
